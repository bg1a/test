#ifndef __MAIN_H__
#define __MAIN_H__

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/irq.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>
#include <linux/errno.h>
#include <asm/uaccess.h>

#include "adt.h"

#define MOD_MINOR_NUM 0
#define MOD_CNT 1

#define MOD_IOC_MAGIC_NUM 'm'
#define MOD_IOC_CLEAR_APP 	_IO(MOD_IOC_MAGIC_NUM, 0)
#define MOD_IOC_CLEAR_DB	_IO(MOD_IOC_MAGIC_NUM, 1)
#define MOD_IOC_MAX_NR		1

#define CMD_CHAR "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-"

#define XSTR(_str) #_str
#define STR(_str) XSTR(_str)

#ifdef MOD_DEBUG
#define MOD_PRINT(_fmt, args...) do{printk(KERN_ERR _fmt, ##args);}while(0)
#else
#define MOD_PRINT(_fmt, args...)
#endif

typedef struct _mod_priv{
	int cmd_len;
	char *cmd;
	char *path;
	struct cdev cdev;
    int buf_len;
    char *buf;
    char *off;
}mod_priv;

int enoughCap(char *a, char *b, int c, long priv);
void printApp(char *path, char *ap, int cnt, long priv);
void printCmdApp(char *path, char *ap, int cnt, long priv);
int cmdSanity(char *buf, int buf_len);

#endif /* __MAIN_H__ */
