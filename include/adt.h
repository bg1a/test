#ifndef __ADT_H__
#define __ADT_H__

#include "os_abl.h"

#ifdef ADT_DEBUG
#define ADT_PRINT(_fmt, args...) ABL_PRINT(_fmt, ##args)
#else
#define ADT_PRINT(_fmt, ...)
#endif /* ADT_DEBUG */

typedef struct _app_ent{
    struct _app_ent *next;
    struct _app_ent *back;
    char *cmd;
	char *path;
	int cnt;
}app_ent;

typedef struct _node{
	char key;
    union{
		struct{
			struct _node *l;
			struct _node *m;
			struct _node *r;
		}lnk;
		app_ent ap;
    };
}node;

typedef struct _app_data{
    node TST;
    app_ent deq;
    OS_SPL(lock);
}app_data;


typedef void (*alg)(char* , char *, int, long);
typedef int (*pred)(char* , char *, int, long);

int addPathToCntr(char* path);
void foreachApp_cond(alg a, long apriv, pred pr, long ppriv);
void foreachApp(alg a, long apriv);
void deleteRecord(char *path);
void deleteAllRecords(void);

#endif /* __ADT_H__ */
