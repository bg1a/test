#ifndef __OS_ABL__
#define __OS_ABL__

#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/spinlock.h>

#define OS_ALLOC_MEM(_size) kmalloc((_size), GFP_ATOMIC)
#define OS_ZALLOC_MEM(_size) kzalloc((_size), GFP_ATOMIC)
#define OS_FREE_MEM(_ptr) do{kfree(_ptr);}while(0)

#define OS_SPL(_lock) spinlock_t _lock
#define OS_SPL_UNLOCKED(_lock) __SPIN_LOCK_UNLOCKED(_lock)
#define OS_SPL_LOCK(_lock_ptr)	do{spin_lock(_lock_ptr);}while(0)
#define OS_SPL_UNLOCK(_lock_ptr) do{spin_unlock(_lock_ptr);}while(0)

#define ABL_PRINT(_fmt, args...) do{printk(KERN_ERR _fmt, ##args);}while(0)

#endif /* __OS_ABL__ */
