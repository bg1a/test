Application Monitor (apps)
==========================

Overview
--------
This monitor keeps track of an application has been run on a host.

Details
-------
It consists of next parts:

	1. driver (apps)
	2. main script (run.sh)
	3. user space control utility (ctrl)
	
To start with run *make* in the current folder. Then launch *run.sh* script.
In a commander menu choose the "Show applications data" item. You will
get an information of an application counter (shows how many times application 
was launched), an application name and it's path. Press "o+Enter" to get a
Options menu. Choose "Get application data" and enter the name of
application you want to filter. If you enter the empty string here
you will get an entire database. Another options allows to clear the
application data or whole database (by means of *ctrl* utility).



