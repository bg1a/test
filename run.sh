#! /bin/sh
MODULE="apps"
INODE_DEVICE="$MODULE"
ACCESS_UMASK="777"
PWD=$(pwd)

# Remove outdated prints
# sudo dmesg -c > /dev/null

check_module()
{
	lsmod | grep -q "$MODULE"
	return $?
}

remove_module()
{
	echo "Removing driver ..."
	sudo rmmod "$MODULE"
	return $?
}

insert_module()
{
	OK=0
	res=1
	if [ -f "${PWD}/${MODULE}.ko" ]; then
		sudo insmod "${PWD}/${MODULE}.ko"
		res=$?
		if [ $res -eq $OK ]; then
			MAJOR_NUM=$(cat /proc/devices | sed -n 's/^\([0-9]\+\).*'"$MODULE"'.*$/\1/p')
			if [ -c "$PWD/$INODE_DEVICE" ]; then
				sudo rm -f "$PWD/$INODE_DEVICE"
			fi
			sudo mknod "$PWD/$INODE_DEVICE" -m "$ACCESS_UMASK" c $MAJOR_NUM 0
			res=$?
		else
			echo "Unable to load the driver module!"
		fi
	else
		echo "Can't find driver module: ${PWD}/${MODULE}.ko"
	fi
	return $res
}

MENU=main_menu
PREV_MENU=""
CHOICE=""
draw()
{
	while : ; do
		echo "\033c"		
		if [ -n "$MENU" ]; then
			$MENU
		else
			break
		fi
	done	
}

process_input()
{
	cat <<-EOF
		
		Press "b+Enter" to return or "q+Enter" to exit.
		
	EOF

	read CHOICE
	case "$CHOICE" in
	[bB] ) MENU="$PREV_MENU" ;;
	[qQ] ) MENU="" ;;
	* ) return 0 ;;
	esac
	return 1
}

force_exit()
{
	exit
}

main()
{
	input=""
	OK=0
	res=1
	if check_module ; then
		echo "There is an active module $MODULE"
		echo "Do you wish to reload it? (y/n)"
		read input
		case "$input" in
		[yY] ) 
			if remove_module ; then
				insert_module
				res=$?			
			else	
				echo "Unable to remove $MODULE"
				echo "Try to remove it manually"
				sleep 2
			fi ;;
		* ) echo "Trying to work with the loaded module"
			res=$OK 
			sleep 2 ;;
		esac
	else
        echo "Loading driver ..."
		insert_module
		res=$?
		sleep 2
	fi
	
	if [ $res -eq $OK ] ; then
		draw
	else			
		prompt_delay "Press any key to exit"
	fi
}

change_menu()
{
	new_menu="$1"
	if [ -n "$new_menu" -a "$MENU" != "$new_menu" ]; then
		PREV_MENU=$MENU
		MENU=$new_menu
	fi
}

prompt_delay()
{
	dummy=""
	read -p"$*" dummy 
}

main_menu()
{
	if check_module ; then
		cat <<-EOF
			1. Help
			2. Show applications data 
			3. Remove module
		EOF
	
		if process_input ; then	
			case $CHOICE in
			"1") change_menu help_menu ;;
			"2") change_menu showApp_menu ;;
			"3") change_menu removeMod_menu ;;
			esac
		fi
	else
		echo "Module has been unloaded!"
		echo "Do you want to load it? (y/n)"
		if process_input ; then
			case $CHOICE in
			[yY] ) 
				if ! insert_module ; then
					echo "Error while loading driver!"
					prompt_delay "Press any key to exit"
					force_exit
				fi ;;
			* ) force_exit ;;
			esac
		fi
	fi
}

help_menu()
{
	cat <<-EOF
		Script utility to control the driver and user 
		space application.
		In the "Show application data" menu first column 
		means how many times application was launched, 
		second one shows application name and the
		third - application path.
	EOF
	process_input
}

showApp_menu()
{
	ch_file="${PWD}/$INODE_DEVICE"
	ctrl_file="${PWD}/ctrl"
	app_name=""
	
	cat <<-EOF
		Press Enter to refresh or "o+Enter"
		to get a submenu for options.
		Press "h+Enter" to show help information.
		
	EOF
	
	if [ -c "$ch_file" ]; then
		cat "$ch_file" 
	else
		echo "Character device does not exist!"
	fi 
	
	if [ ! -f "$ctrl_file" ]; then
		echo "Can't find $ctrl_file"
	fi
	
	if process_input ; then
		case "$CHOICE" in
		[\n] ) change_menu showApp_menu ;;
		[hH] ) change_menu help_menu ;;
		[oO] ) echo "Options menu."
			cat <<-EOF
				1. Get application data
				2. Clear application data
				3. Reset database	
				Tip: to get an entire database, enter an empty string in
				"Get application data" menu.
			EOF
			if process_input ; then
				case "$CHOICE" in
				"1" ) echo "Enter the application name"
					read app_name
					if [ -c "$ch_file" ]; then
						echo "$app_name" > "$ch_file"
					fi ;;
				"2" ) echo "Removing application data ..."
					if [ -f "$ctrl_file" ]; then
						$ctrl_file -r 
					fi ;;
				"3" ) echo "Clearing database ..." 
					if [ -f "$ctrl_file" ]; then
						$ctrl_file -c 
					fi ;;
				esac
			fi
		esac
	fi
}

removeMod_menu()
{
	echo "Do you wish to remove module? (y/n)"
	if process_input ; then
		case "$CHOICE" in
		[yY] ) 
			if ! remove_module ; then
				echo "Unable to remove $MODULE"
			fi 
			sleep 2 ;;
		* ) : ;;
		esac
	fi
}

main 
