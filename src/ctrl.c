#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

#define XSTR(_str) #_str
#define STR(_str) XSTR(_str)

#define IOC_MAGIC_NUM 'm'
#define IOC_CLEAR_APP 	_IO(IOC_MAGIC_NUM, 0)
#define IOC_CLEAR_DB	_IO(IOC_MAGIC_NUM, 1)

void usage(char *name)
{

	printf("This is a tiny utility to control the driver.\n");
	printf("Usage:");
	printf("\t%s -r - remove an application data,\n", name);
	printf("\t%s -c - clear the entire database.\n", name);
}

int main (int argc, char *argv[])
{
	int fd = 0;
	int request = 0;
	int c = 0;

	if((argc < 2) || (!strcmp(argv[1], "--help")))
	{
		usage(argv[0]);
	}else{
		fd = open(STR(DEV_NAME), O_WRONLY);
		if(fd)
		{
			while((c = getopt(argc, argv, "rc")) != -1)
			{
				switch(c)
				{
					case 'r': request = IOC_CLEAR_APP; break;
					case 'c': request = IOC_CLEAR_DB; break;
					default: usage(argv[0]);
				}
				break;
			}
			
			if(request)		
			{
				if(-1 == ioctl(fd, request))
					perror("ioctl");
			}else{
				printf("Unknown ioctl request!\n");
			}
				
			close(fd);
		}else{
			perror("open");
		}	
	}	
	
	return 0;
}

