/*
 * @Author: bg1a bg1a@yandex.ru
 *
 * @File: main.c
 *
 * @Synopsis:
 *  23/06/2014: First version. TODO: add a processing of position pointer
 *          in a read method. Now, it was tested only with cat utility, and
 *          it's typical buffer size of 65536 bytes. It is quite enough.
 *          Test driver with another kernels. Initially it was tested with
 *          3.13.0-29 kernel.
 *
 */

#include "main.h"

MODULE_LICENSE("Dual BSD/GPL");

static dev_t dev_num = 0;
static mod_priv mp;

/*
 * mdo_execve
 * An entry probe fo execve_common system call
 * @param: filename, as isl
 * @param: argv, application parameters
 * @param: envp, environmental parameters
 * @return: hopefully formal return is unreached
 */
static int mdo_execve(const char * filename,
                     const char __user * const __user * argv,
                     const char __user * const __user * envp)

{
    int len = strlen(filename);
    char *path = kzalloc(sizeof(*path) * len + 1, GFP_ATOMIC);

    memcpy(path, filename, len);
    if(!addPathToCntr(path))
    	kfree(path);
    MOD_PRINT("---> execve for %s from %s\n", filename, current->comm);
	jprobe_return();
	return 0;
} /* mdo_execve() */

static struct jprobe mod_jprobe = {
	.entry = mdo_execve,
	.kp = {
		.symbol_name = "do_execve_common",
	},
};

/*
 * mod_open
 * An open method of a driver
 * @param: inode, inode that corresponds to the char driver
 * @param: filp, structure representing an open file
 * @return: 0 on success
 */
int mod_open(struct inode *inode, struct file *filp)
{
	mod_priv *mp = container_of(inode->i_cdev, mod_priv, cdev);
	filp->private_data = mp;
	MOD_PRINT("-----> In open\n");
	return nonseekable_open(inode, filp);
} /* mod_open() */

/*
 * mod_release
 * A release method of a driver
 * @param: inode, inode that corresponds to the char driver
 * @param: filp, structure representing an open file
 * @return: 0 on success
 */
int mod_release(struct inode *inode, struct file *filp)
{
	MOD_PRINT("-----> In release\n");
	return 0;
} /* mod_release() */

/*
 * mod_read
 * A read method of a driver
 * @param: filp, structure representing an open file
 * @param: buf, user-space buffer for data
 * @param: count, buffer size
 * @param: f_pos, pointer to a variable containing position offset
 * @return: number of bytes successfully read from a device
 */
ssize_t mod_read(struct file* filp, char __user* buf, size_t count, loff_t* f_pos)
{
    mod_priv* mp = (mod_priv *)filp->private_data;
    ssize_t data_len = 0;

    MOD_PRINT("-----> In read, count is: %d\n", count);
	if(!*f_pos)
	{
        if(mp->buf)
        {
            kfree(mp->buf);
            mp->buf = NULL;
        }
		mp->buf = kmalloc(sizeof(*mp->buf) * count, GFP_ATOMIC);
		if(mp->buf)
		{
		    mp->buf_len = count;
		    mp->off = mp->buf;
			MOD_PRINT("---> mp: %p\n", mp);
			if(mp->cmd)
		    	foreachApp_cond(printCmdApp, (long) mp, enoughCap, (long) mp);
		    else
		    	foreachApp_cond(printApp, (long) mp, enoughCap, (long) mp);
		    data_len = (mp->off - mp->buf) < count ? (mp->off - mp->buf) : count;
			MOD_PRINT("---> mp->buf: %p, mp->off: %p, mp->buf_len: %d, text: %s\n", mp->buf, mp->off, mp->buf_len, mp->buf);
		    if(copy_to_user(buf, mp->buf, data_len))
                return -EFAULT;
		    kfree(mp->buf);
            mp->buf = NULL;
		    *f_pos = data_len;
		}
	}else{
		*f_pos = 0;
		data_len = 0;
	}
    return data_len;
} /* mod_read() */

/*
 * enoughCap
 * Predicate, calculates a free space in a buffer
 * @param: a, dummy pointer to satisfy demands of foreachApp algorithm
 * @param: b, dummy pointer
 * @param: c, dummy pointer
 * @param: priv, some extra data for the predicate
 * return: 1 if enough space in a buffer, 0 otherwise
 */
int enoughCap(char *a, char *b, int c, long priv)
{
	mod_priv *mp = (mod_priv *) priv;
	char *buf = mp->buf;
	char *off = mp->off;
	int len = mp->buf_len;

    MOD_PRINT("---> mp: %p\n", mp);
	MOD_PRINT("---> mp->buf: %p, mp->off: %p, mp->buf_len: %d, %s\n", mp->buf, mp->off, mp->buf_len, __func__);
	return (buf + len - off) > 0 ? 1 : 0;
} /* enoughCap() */

/* printApp
 * Algorithm helper, prints all application name, path and counter to the buffer
 * @param: path, application path
 * @param: ap, application name
 * @param: cnt, counter
 * @param: priv, extra data
 * @return: nothing
 */
void printApp(char *path, char *ap, int cnt, long priv)
{
	mod_priv *mp = (mod_priv *) priv;
	char *buf = mp->buf;
	char *off = mp->off;
	int len = mp->buf_len;

	MOD_PRINT("---> current path: %s app: %s cnt: %d\n", path, ap, cnt);
	mp->off += snprintf(off, (buf + len - off), "%d\t%s\t\t%s\n", cnt, ap, path);
} /* printApp() */

/*
 * printCmdApp
 * Prints application data whose name is set in mp->cmd
 * @param: path, application path
 * @param: ap, application name
 * @param: cnt, counter
 * @param: priv, extra data
 * @return: nothing
 */
void printCmdApp(char *path, char *ap, int cnt, long priv)
{
	mod_priv *mp = (mod_priv *) priv;
	char *buf = mp->buf;
	char *off = mp->off;
	char *cmd = mp->cmd;
	int len = mp->buf_len;

	MOD_PRINT("---> current path: %s app: %s cnt: %d\n", path, ap, cnt);
	if(!strcmp(cmd, ap))
		mp->off += snprintf(off, (buf + len - off), "%d\t%s\t\t%s\n", cnt, ap, path);
} /* printCmdApp() */

/*
 * cmdSanity
 * Checks buffer data for a valid filename
 * @param: buf, a buffer with filename
 * @param: buf_len, buffer length
 * @return: 1 if there is any valid filename, 0 otherwise
 */
int cmdSanity(char *buf, int buf_len)
{
	int spn = strspn(buf, CMD_CHAR);

	if(spn && (buf_len - spn))
			snprintf(buf + spn, buf_len - spn, "%c", '\0');

	return (spn ? 1 : 0);
} /* cmdSanity() */

/*
 * searchApp
 * Gets an application path from database
 * @param: path, a path from application entry of a database
 * @param: ap, application name from database
 * @param: cnt, application entry counter
 * @param: priv, extra data
 * @return: nothing
 */
void searchApp(char *path, char *ap, int cnt, long priv)
{
	mod_priv *mp = (mod_priv *) priv;

	if(!strcmp(ap, mp->cmd))
		mp->path = path;
} /* searchApp() */

/*
 * mod_write
 * A write method of a driver
 * @param: filp, a structure of an open char device file
 * @param: buf, a buffer containing some data to write
 * @param: count, a buffer size
 * @param: f_pos, a position offset
 * @return: number of successfully written bytes
 */
ssize_t mod_write(struct file* filp, const char __user* buf, size_t count, loff_t* f_pos)
{
	char *ptr;
	mod_priv* mp = filp->private_data;

	MOD_PRINT("-----> In write\n");
	if(mp->cmd)
	{
		kfree(mp->cmd);
		mp->cmd = NULL;
		mp->cmd_len = 0;
	}
	ptr = kzalloc(count*sizeof(*ptr) + 1, GFP_ATOMIC);
	if(ptr)
	{
		if(copy_from_user(ptr, buf, count))
        {
            kfree(ptr);
            return -EFAULT;
        }
		if(cmdSanity(ptr, count))
		{
			mp->cmd = ptr;
			mp->cmd_len = count;
			mp->path = NULL;
			foreachApp(searchApp, (long) mp);
			MOD_PRINT("---> path: %s\n", mp->path);
		}else{
			printk(KERN_ERR "---> bad filename in %s\n", __func__);
			kfree(ptr);
		}
		printk(KERN_ERR "command: %s\n", mp->cmd ? : "not acknowledged");
	}else{
		printk(KERN_ERR "Unable to allocate %d bytes\n", count);
	}
    MOD_PRINT("<----- In write\n");
	return count;
} /* mod_write() */

/*
 * mod_ioctl
 * IOCTL method of a driver
 * @param: filp, an open file structure
 * @param: cmd, a command code
 * @param: arg, an extra argument
 * @return: 0 on success or an error code
 */
long mod_ioctl(struct file* filp, unsigned int cmd, unsigned long arg)
{
	mod_priv* mp = filp->private_data;

	MOD_PRINT("-----> In ioctl\n");
	if (_IOC_TYPE(cmd) != MOD_IOC_MAGIC_NUM) return -ENOTTY;
	if (_IOC_NR(cmd) > MOD_IOC_MAX_NR) return -ENOTTY;
	switch(cmd)
	{
		case MOD_IOC_CLEAR_APP:
			if(mp->path)
				deleteRecord(mp->path);
			break;
		case MOD_IOC_CLEAR_DB:
			deleteAllRecords();
			break;
		default:
			return -ENOTTY;
	}
	MOD_PRINT("<----- In ioctl\n");
	return 0;
} /* mod_ioctl() */

struct file_operations mod_fops = {
	.owner = THIS_MODULE,
	.open = mod_open,
	.release = mod_release,
	.read = mod_read,
	.llseek = noop_llseek,
	.write = mod_write,
	.unlocked_ioctl = mod_ioctl,
};

/*
 * mod_init
 * Driver init function
 * @return: an error code
 */
static int __init mod_init(void)
{
	int res = 0;

    printk(KERN_INFO "[%s]: Loading module ...\n", STR(DEV_NAME));
	res = alloc_chrdev_region(&dev_num, MOD_MINOR_NUM, MOD_CNT, STR(DEV_NAME));
	if(res < 0)
	{
		printk(KERN_ERR "[%s]: Unable to allocate a char device!\n", STR(DEV_NAME));
		return res;
	}else{
		printk(KERN_INFO "[%s]: dev num is: %d\n", STR(DEV_NAME), MAJOR(dev_num));
	}
	mp.cmd = NULL;
	mp.cmd_len = 0;
    mp.path = NULL;
    mp.buf = NULL;
    mp.buf_len = 0;
    mp.off = NULL;

	cdev_init(&mp.cdev, &mod_fops);
	mp.cdev.owner = THIS_MODULE;
	mp.cdev.ops = &mod_fops;
	res = cdev_add(&mp.cdev, dev_num, 1);
	if(res)
    {
		printk(KERN_ERR "[%s]: Unable to add a device, %d\n", STR(DEV_NAME), res);
        return res;
    }

	if((res = register_jprobe(&mod_jprobe)) < 0)
    {
		printk(KERN_ERR "[%s]: Unable to register probe!\n", STR(DEV_NAME));
    }else{
	    printk(KERN_INFO "[%s]: jprobe is planted at %p, handler addr %p\n", STR(DEV_NAME),
		    mod_jprobe.kp.addr, mod_jprobe.entry);
    }

	return res;
} /* mod_init() */

/*
 * mod_exit
 * Driver exit routine
 */
static void mod_exit(void)
{
    unregister_jprobe(&mod_jprobe);
	deleteAllRecords();
	if(mp.cmd)
		kfree(mp.cmd);
    if(mp.buf)
        kfree(mp.buf);
	cdev_del(&mp.cdev);
	unregister_chrdev_region(dev_num, 1);
	printk(KERN_ERR "[%s]: Module exit\n", STR(DEV_NAME));
} /* mod_exit() */

module_init(mod_init);
module_exit(mod_exit);
