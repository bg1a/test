#include "adt.h"

#ifdef ADT_DEBUG
static int debugCnt = 0;
#endif /* ADT_DEBUG */

static app_data dataContainer = {
    .TST = {
        .key = '/',
        .lnk = {
            .l = NULL,
            .m = NULL,
            .r = NULL,
        },
    },
    .deq = {0},
    .lock = OS_SPL_UNLOCKED(dataContainer.lock),
};

static node* newNode(char key);
static int delNode(node* link);
static void deletePath(char *path);

/*
 * newNode
 * Add a new node to Ternary Search Tree
 * @param: key, a character for the node
 * @return: pointer to the new node
 */
static node* newNode(char key)
{
    node * x = OS_ZALLOC_MEM(sizeof(*x));
	x->key = key;
#ifdef ADT_DEBUG
    debugCnt++;
#endif /* ADT_DEBUG */
	return x;
} /* newNode() */

/*
 * addPathToCntr
 * Add an application path to the container (TST and dequeue)
 * @param: path, an absolute path to the application
 * @return: 1 if a new path has been added to the container, or 0
 *          if path exists in container
 */
int addPathToCntr(char* path)
{
	char *pwd = path;
    char *app = NULL;
    node* cl = &dataContainer.TST;
    node **ln = NULL;
    int res = 0;
#ifdef ADT_DEBUG
	int cnt = 0;
#endif /* ADT_DEBUG */

	OS_SPL_LOCK(&dataContainer.lock);
    for(; *pwd != '\0'; cl = *ln)
	{
        ADT_PRINT("---> iteration: %d, *pwd: %c, cl: %p\n", cnt++, *pwd, cl);
        if(!cl)
            *ln = cl = newNode(*pwd);
        if(*pwd == '/')
               app = pwd + 1;

        if(*pwd < cl->key)
        {
            ADT_PRINT("---> [left] key: %c\n", cl->key);
            ln = &cl->lnk.l;
        }else if(*pwd == cl->key){
            ADT_PRINT("---> [middle] key: %c\n", cl->key);
            ln = &cl->lnk.m;
            pwd++;
        }else{      // if(*pwd > cl->key)
            ADT_PRINT("---> [right] key: %c\n", cl->key);
            ln = &cl->lnk.r;
        }
    }
    if(!*ln)
    {
        ADT_PRINT("---> adding a new node\n");
        *ln = cl = newNode('\0');
        cl->ap.path = path;
        cl->ap.cmd = app;
        cl->ap.next = dataContainer.deq.next;
        cl->ap.back = &dataContainer.deq;
        dataContainer.deq.next = &cl->ap;
        if(cl->ap.next)
            cl->ap.next->back = &cl->ap;
        ADT_PRINT("<--- adding a new node\n");
		res = 1;
    }
    (*ln)->ap.cnt++;
    OS_SPL_UNLOCK(&dataContainer.lock);
    
	return res;
} /* addPathToCntr() */

/*
 * foreachApp_cond
 * Foreach algorithm conditional
 * @param: a, algoritm helper
 * @param: apriv, private data for algorithm helper
 * @param: pr, predicate
 * @param: ppriv, predicate private
 */
void foreachApp_cond(alg a, long apriv, pred pr, long ppriv)
{
    app_ent *next = dataContainer.deq.next;
	
	OS_SPL_LOCK(&dataContainer.lock);
    while(next && pr(next->path, next->cmd, next->cnt, ppriv)) 
	{
        a(next->path, next->cmd, next->cnt, apriv);
        next = next->next;
    }   
    OS_SPL_UNLOCK(&dataContainer.lock);
} /* foreachApp_cond() */

/*
 * foreachApp
 * Foreach algorithm
 * @param: a, algorithm
 * @param: apriv, algorithm private data
 */
void foreachApp(alg a, long apriv)
{
    app_ent *next = dataContainer.deq.next;
	
	OS_SPL_LOCK(&dataContainer.lock);
    for(; next; next = next->next) 
        a(next->path, next->cmd, next->cnt, apriv);

    OS_SPL_UNLOCK(&dataContainer.lock);
} /* foreachApp() */

/*
 * delNode
 * Delete node
 * @param: link, node pointer
 * @return: 1 if it was deleted a terminal '\0' node,
 *          0 otherwise
 */
static int delNode(node* link)
{
	app_ent *ap = NULL;
	int res = 0;

	if(link->key == '\0')
	{
		ADT_PRINT("---> removing zero node\n");
		
		res = 1;
		ap = &link->ap;
		if(ap->path)
		{
			ADT_PRINT("---> removing path: %s\n", ap->path);
		    OS_FREE_MEM(ap->path);
		    ap->path = NULL;
		}
		ap->back->next = ap->next;
		if(ap->next)
			ap->next->back = ap->back;
	}else{
		ADT_PRINT("---> removing common node\n");
		OS_FREE_MEM(link);		
	}
#ifdef ADT_DEBUG
	debugCnt--;
#endif /* ADT_DEBUG */
	
	return res;
} /* delNode() */

/*
 * deletePath
 * Delete path record from database
 * @param: path, pointer to the path record
 */
static void deletePath(char *path)
{
	char *pwd = path;
    node* cl = &dataContainer.TST;
    node **ln = NULL;
    node **mrk = &cl->lnk.m;
	int cnt = 0;

    for(; *pwd != '\0'; cl = *ln)
	{
        ADT_PRINT("---> iteration: %d, *pwd: %c, cl: %p\n", cnt++, *pwd, cl);
        if(*pwd < cl->key)
        {
            ADT_PRINT("---> [left] key: %c\n", cl->key);
            ln = &cl->lnk.l;
            if(cl->lnk.r || cl->lnk.m)
            	mrk = ln;
        }else if(*pwd == cl->key){
            ADT_PRINT("---> [middle] key: %c\n", cl->key);
            ADT_PRINT("---> child node left: %c, middle: %c, right: %c\n", (cl->lnk.l ? cl->lnk.l->key : ' '),
            		(cl->lnk.m ? cl->lnk.m->key : ' '), (cl->lnk.r ? cl->lnk.r->key : ' '));
            pwd++;            
            ln = &cl->lnk.m;
            if(cl->lnk.r || cl->lnk.l)
            	mrk = ln;
        }else{      /* if(*pwd > cl->key) */
            ADT_PRINT("---> [right] key: %c\n", cl->key);
            ln = &cl->lnk.r;
            if(cl->lnk.l || cl->lnk.m)
            	mrk = ln;
        }
    }
    ADT_PRINT("---> mrk: %p\n", mrk);
    
    if(*mrk)
    {
    	cnt = 0;
    	cl = *mrk;
    	while(cl) 
    	{
    		ADT_PRINT("---> key for delete: %c, iteration: %d\n", cl->key, cnt++);
    		*mrk = NULL;
			if(cl->lnk.m)
				mrk = &cl->lnk.m;
			else if(cl->lnk.r)
				mrk = &cl->lnk.r;
			else
				mrk = &cl->lnk.l;
				
			if(delNode(cl))
				break;
			cl = *mrk;
    	}
    }
} /* deletePath() */

/*
 * deleteRecord
 * Removes path record, a wrapper to deletePath() 
 * @param: path, path pointer
 */
void deleteRecord(char *path)
{
	OS_SPL_LOCK(&dataContainer.lock);
	deletePath(path);
	OS_SPL_UNLOCK(&dataContainer.lock);
} /* deleteRecord() */

/*
 * deleteAllRecords
 * Clears the entire database
 */
void deleteAllRecords(void)
{
	app_ent **ap = &dataContainer.deq.next;

	OS_SPL_LOCK(&dataContainer.lock);
	while(*ap)
		deletePath((*ap)->path);
	OS_SPL_UNLOCK(&dataContainer.lock);
	ADT_PRINT("---> debug cnt is: %d in  %s\n", debugCnt, __func__);
} /* deleteAllRecords() */

