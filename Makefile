TARGET := apps
INC_DIR := $(PWD)/include
KDIR := /lib/modules/$(shell uname -r)/build
PHONY :=

COMMON_FLAGS := -DDEV_NAME=$(TARGET)

SRC := main.c adt.c
SRC_DIR := src
OBJ := $(addprefix $(SRC_DIR)/,$(SRC:.c=.o))

$(TARGET)_FLAGS := -g3 -gdwarf-2 $(COMMON_FLAGS)
# Uncomment to get a debug info in dmesg
# $(TARGET)_FLAGS += -DMOD_DEBUG -DADT_DEBUG
obj-m := $(TARGET).o
$(TARGET)-objs = $(OBJ)
ccflags-y := -I$(INC_DIR) $($(TARGET)_FLAGS)

US_TARGET := ctrl
US_SRC := ctrl.c
US_SRC_DIR := src
$(US_TARGET)_FLAGS := $(COMMON_FLAGS)

PHONY += all
all:
	$(CC) $($(US_TARGET)_FLAGS) -o $(US_TARGET) $(addprefix $(US_SRC_DIR)/,$(US_SRC))
	$(MAKE) -C $(KDIR) M=$(PWD) modules 

PHONY += clean
clean:
	@rm -f $(US_TARGET) $(TARGET) 
	$(MAKE) -C $(KDIR) M=$(PWD) clean 

.PHONY: $(PHONY)
